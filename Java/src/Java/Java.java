package Java;

import java.io.BufferedReader;
import java.io.FileReader;

public class Java {
	public static void main(String[] args) {
		try(BufferedReader reader = new BufferedReader(new FileReader("../Imena.txt"))) {
			StringBuilder sb = new StringBuilder();
			String line = reader.readLine();
			
			while(line != null) {
				sb.append(line + "\n");
				line = reader.readLine();
			}
			System.out.println(sb.toString());
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}
}
