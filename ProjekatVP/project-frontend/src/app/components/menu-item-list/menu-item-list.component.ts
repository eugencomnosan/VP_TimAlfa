import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication-service.service';
import { Router } from '@angular/router';
import { MenuItemService } from '../../services/menu-item-service.service';
import { Page } from '../../models/Page';
import { MenuItem } from '../../models/MenuItem';
import { Filter } from '../../models/FIlter';


@Component({
  selector: 'app-menu-item-list',
  templateUrl: './menu-item-list.component.html',
  styleUrls: ['./menu-item-list.component.css']
})
export class MenuItemListComponent implements OnInit {
  page:Page<MenuItem>;
  currentPageNumber:number;
  totalPages:number;
  itemsPerPage = 10;
  

  filter:Filter={
    name:"",
    categoryId:0
  };
  
  constructor(
    private authService : AuthenticationService,
    private router:Router,
    private menuItemService:MenuItemService
  ) { }

  ngOnInit() {
    this.currentPageNumber=0;
    this.loadData();
  }
  
  search(filter:Filter){
    console.log(filter);
    this.filter=filter
    this.menuItemService.getAll(this.currentPageNumber,this.itemsPerPage,filter.name,filter.categoryId).subscribe(data => {
      this.page=data;
      this.totalPages=data.totalPages;

    })
  }

  loadData(){
    // this.menuItemService.getAll(this.currentPageNumber,this.itemsPerPage,this.filter.name,this.filter.categoryId).subscribe(data => {
    //   this.page=data;
    //   this.totalPages=data.totalPages;
    // })
    this.search(this.filter);
  }
  delete(id:number){
    this.menuItemService.delete(id).subscribe(()=> this.loadData())
  }

  changePage(i:number){
    this.currentPageNumber+=i;
    this.loadData();
  }
  itemsPerPageChanged(i:number){
    this.itemsPerPage=i;
    this.loadData();
  }
  isLoggedIn(): boolean{
    return this.authService.isLoggedIn();
  }
  isAdmin(){
    return this.authService.isAdmin();
  }
}
