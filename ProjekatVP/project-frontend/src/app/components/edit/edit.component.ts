import { Component as c, OnInit } from "@angular/core";

import { AuthenticationService } from "../../services/authentication-service.service";
import { Observable } from "rxjs/Observable";
import { Input } from "@angular/core";
import { Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "../../models/Page";
import { MenuItem } from "../../models/MenuItem";
import { Category } from "../../models/Category";
import { CategoryService } from "../../services/category-service.service";
import { MenuItemService } from "../../services/menu-item-service.service";


@c({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"]
})
export class Edit {
  item: MenuItem
  dataLoaded = false;
  categories: Category[];


  constructor(
    private categoryService: CategoryService,
    private menuItemService: MenuItemService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.categoryService.getAll().subscribe((data) => this.categories = data)
    this.route.params.subscribe(data => {
      this.menuItemService.get(data["id"]).subscribe(data => {
        this.item = data;
        this.dataLoaded = true;
      });
    });

  }

  save() {
    this.menuItemService.save(this.item).subscribe(
      (yes) => {
        
    this.router.navigate(["/menu"]);
    
      },
      (no) => {
        alert("Error");
      }
    );
  }

  byId(brand1: Category, brand2: Category) {
    if (brand1 && brand2) {
      return brand1.id === brand2.id;
    }
  }

}

