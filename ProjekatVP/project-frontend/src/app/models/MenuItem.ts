import { Category } from "./Category";


export interface MenuItem {
    id?: number;
    category: Category;
    name: string;
    price: number;
}