package project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import project.model.MenuItem;
import project.repository.MenuItemRepository;

@Component
public class MenuItemService {
	@Autowired
	MenuItemRepository menuItemRepository;

	public List<MenuItem> findAll() {
		return menuItemRepository.findAll();
	}

	public Page<MenuItem> findAll(Pageable page, String name,Long id) {
		if(id ==0 )
		return menuItemRepository.getByNameContains(page, name);
		else {
			return  menuItemRepository.getByNameContainsAndCategoryId(page, name, id);
		}
	}

	public MenuItem findOne(Long id) {
		return menuItemRepository.findOne(id);

	}

	public void delete(Long id) {
		menuItemRepository.delete(id);
	}

	public MenuItem save(MenuItem menuItem) {
		return menuItemRepository.save(menuItem);
	}

	public List<MenuItem> findByCategoryId(Long catId) {
		return menuItemRepository.findByCategoryId(catId);
	}

	// public List<MenuItem> getByNameContains(String name) {
	// return menuItemRepository.getByNameContains(name);
	// }

}
