package project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.ResponseDTO;
import project.model.MenuItem;
import project.service.MenuItemService;
@RestController
public class MenuItemController {
	@Autowired
	MenuItemService menuItemService;

	@GetMapping(value = "/api/menuItems")
	public ResponseEntity<Page<MenuItem>> getMenuItemsPage(Pageable page,String name,Long id) {
		Page<MenuItem> menuItems = menuItemService.findAll(page,name,id);
		

		return new ResponseEntity<Page<MenuItem>>(menuItems, HttpStatus.OK);
	}

	@GetMapping(value = "/api/menuItems/{id}")
	public ResponseEntity<MenuItem> getOne(@PathVariable Long id) {
		MenuItem project = menuItemService.findOne(id);
		return Optional.ofNullable(project).map(p -> new ResponseEntity<>(project, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	@PreAuthorize("isAuthenticated()")
	@PostMapping(value = "/api/menuItems", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MenuItem> create(@RequestBody MenuItem recMenuItem) {

		MenuItem item = new MenuItem();
		item.setName(recMenuItem.getName());
		item.setCategory(recMenuItem.getCategory());
		item.setPrice(recMenuItem.getPrice());

		return new ResponseEntity<>(menuItemService.save(item), HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyAuthority('Admin')")
	@DeleteMapping(value = "/api/menuItems/{id}")
	public ResponseEntity<ResponseDTO> delete(@PathVariable Long id) {
		MenuItem item = menuItemService.findOne(id);

		if (item != null) {
			menuItemService.delete(id);
			return new ResponseEntity<>(new ResponseDTO("OK"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseDTO("NOT FOUND"), HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(value = "/api/menuItems/{id}")
	public ResponseEntity<MenuItem> update(@PathVariable Long id, @RequestBody MenuItem recMenuItem) {
		MenuItem item = menuItemService.findOne(id);

		if (item != null) {
			
			item.setName(recMenuItem.getName());
			item.setCategory(recMenuItem.getCategory());
			item.setPrice(recMenuItem.getPrice());

			menuItemService.save(item);
			return new ResponseEntity<MenuItem>(item, HttpStatus.OK);
		} else {
			return new ResponseEntity<MenuItem>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/api/menuItems/category/{catId}")
	public ResponseEntity<List<MenuItem>> getMenuItemsByCategory(@PathVariable Long catId) {
		List<MenuItem> menuItems = menuItemService.findByCategoryId(catId);
		return Optional.ofNullable(menuItems).map(p -> new ResponseEntity<>(menuItems, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}
	
	
}
