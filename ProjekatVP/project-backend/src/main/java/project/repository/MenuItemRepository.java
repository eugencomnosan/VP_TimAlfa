package project.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import project.model.MenuItem;

@Component
public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {
	public List<MenuItem> findByCategoryId(Long id);
	public Page<MenuItem> getByNameContains(Pageable page,String name);
	public Page<MenuItem> getByNameContainsAndCategoryId(Pageable page,String name,Long id);
}
