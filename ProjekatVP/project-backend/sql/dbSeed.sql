
CREATE DATABASE project;
USE project;

insert into category (id, name) values (1,"Food");
insert into category (id, name) values (2,"Drink");


insert into menu_item(name,category_id,price) values ("piza",1,213);
insert into menu_item(name,category_id,price) values ("1",1,213);
insert into menu_item(name,category_id,price) values ("2",1,213);
insert into menu_item(name,category_id,price) values ("3",1,213);
insert into menu_item(name,category_id,price) values ("4",1,213);
insert into menu_item(name,category_id,price) values ("slatka pica",1,213);
insert into menu_item(name,category_id,price) values ("piza",1,213);



-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password ,role) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password,  role) values 
	('stanar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'User');
